   
    cd LAY2024  ## Have to be here for this to work ############

    ##  SC[ hkmf-mini] #########################################
    ## SEE[ hkmf-c11 ] for reference:  #########################
    ############################################################
    gcc                                                        \
        -x c                                                   \
        -c "./LAY2024.C11"                                     \
        -o object_file.o                                       \
                                                               \
            -Werror                                            \
            -Wfatal-errors                                     \
            -Wpedantic                                         \
            -Wall                                              \
            -Wextra                                            \
                                                               \
            -fstrict-aliasing                                  \
            -Wstrict-aliasing                                  \
                                                               \
            -D COMPILE_EXE_LAY2024                             \
                                                               \
            -std=c11                                           \
            -m64 ###############################################
                                        ####                ####
    gcc -o LAY2024.exe object_file.o    ####                ####                
    rm                 object_file.o    ####                ####     
         ./LAY2024.exe                  ####                ####
    rm     LAY2024.exe                  ####                ####

    mv   ./IGNORE/BLAKINK.000.PNG   ../BLAKINK.001.PNG
    mv   ./IGNORE/BLAKINK.001.PNG   ../BLAKINK.002.PNG
    mv   ./IGNORE/BLAKINK.002.PNG   ../BLAKINK.003.PNG
    mv   ./IGNORE/BLAKINK.003.PNG   ../BLAKINK.004.PNG

    read -p "[ENTER_TO_EXIT]:"          ####                ####
    ############################################################
